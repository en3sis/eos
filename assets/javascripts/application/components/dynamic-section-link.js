/**
 * On document ready, we'll scan for all h2 that has the class of .anchor.
 * If the element is found, it will generate the anchor link based on the title name
 * If the name is too long or we want a custom anchor text/link, we can add:
 * .h2.anchor(data-tag='abc-abc') <- data-tag atribute will replace the convention.
 */
$(function () {
  const header = document.querySelectorAll('.anchor')
  header.forEach((ele) => {
    const tag = ele.textContent.split(' ').join('-').toLocaleLowerCase()

    const link = document.createElement('a')

    link.href = `#${ele.dataset.tag ? ele.dataset.tag : tag}`
    link.className = 'buttons-btn-vs-link'
    link.id = ele.dataset.tag ? ele.dataset.tag : tag
    link.innerHTML = `<i class="eos-icons eos-18">link</i>`

    return ele.append(link)
  })
})
