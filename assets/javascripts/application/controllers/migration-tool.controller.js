let backupCollectionsList

const requestStatus = {
  isLoading: false,
  isError: false
}

$(function () {
  if (!$('.js-migration-tool')[0]) return

  fetch('/api/mongodb/collections', {
    method: 'get',
    headers: { 'Content-Type': 'application/json' }
  })
    .then((response) => response.json())
    .then((data) => {
      const $devCool = $('.js-dev-col')
      const $prodCool = $('.js-prod-col')

      populateColumn(data.devColl, $devCool, 'dev')
      populateColumn(data.prodColl, $prodCool, 'prod')

      backupCollectionsList = {
        dev: data.devColl.collections,
        prod: data.prodColl.collections
      }
      console.log('backupCollectionsList: ', backupCollectionsList)
    })
    .catch((error) => {
      console.error('Error:', error)
    })
})

const populateColumn = (data, target, type) => {
  target.find('span').append(data.name)
  $('.js-collection-checkbox').on('click', function (e) {
    const item = $(`*[data-name=${e.target.name}]`)
    item.toggleClass('migration-tool-replace')
  })

  if (type === 'dev') {
    data.collections.map((ele, i) => {
      target.find('.form-group').append(`
    <div class='form-check'>
     <input name=${ele} class='form-check-input eos-checkbox js-collection-checkbox' type='checkbox' id=${ele}>
     <label class='form-check-label' for=${ele}>
       ${ele}
     </label>
   </div>`)
    })
  } else {
    data.collections.map((ele, i) => {
      target.find('.list').append(`
      <div class='migration-tool-production-item' data-name='${ele}'>
        ${ele}
    </div>`)
    })
  }
}

function submitCollections () {
  const filteredDev = []
  $('.form-group input:checked').each(function () {
    filteredDev.push($(this).attr('id'))
  })

  const filteredProd = backupCollectionsList.dev.filter(
    (item) => !filteredDev.includes(item)
  )

  requestStatus.isLoading = true
  handleLoading()

  return fetch('/api/mongodb/migration', {
    method: 'post',
    body: JSON.stringify({
      dev: filteredDev,
      prod: filteredProd,
      newDbName: $('#database-name').val()
    }),
    headers: { 'Content-Type': 'application/json' }
  })
    .then((response) => response.json())
    .then((data) => {
      requestStatus.isLoading = false
      requestStatus.isError = false
      handleLoading(data.newDbName)
    })
    .catch((error) => {
      requestStatus.isError = true
      console.error('Error:', error)
    })
}

const handleLoading = (dbname) => {
  const { isLoading, isError } = requestStatus
  console.log('status: ', requestStatus)

  if (isLoading) {
    const target = $('.js-migration-loading')
    target.css('color', 'red')
    target.toggleClass('d-none')
    $('.js-migration-submit').addClass('disabled')
  } else if (!isLoading && !isError) {
    const target = $('.js-migration-success')
    target.toggleClass('d-none')
    target.css('color', 'green')
    $('.js-migration-loading').addClass('d-none')
    target.append(
      `Success! The new database was created. Add <strong> ${dbname} </strong> it to the env variables in <a href="https://dashboard.heroku.com/"> Heroku </a>`
    )
  } else {
    window.alert(
      'Something went wrong =/. Check the Application console for more information'
    )
  }
}
