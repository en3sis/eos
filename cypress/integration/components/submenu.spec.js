describe('submenu', () => {
  before(() => {
    cy.visit('/examples/demo-layout.html')
  })

  it('should position the dropdown to right when windows width is smaller than 480px', () => {
    cy.viewport(480, 750)

    cy.get('.js-submenu-content').should('have.class', 'dropdown-menu-right')

    cy.get('.js-submenu-visible').contains('Submenu 7')
  })

  it('should move elements to the dropdown and add the hide class to them if the submenu is too small to fit all submenu items', () => {
    cy.viewport(1330, 750)

    cy.get('.js-submenu-more-list a').should('have.length', 2)

    cy.get('.js-submenu-visible li a').last().should('have.class', 'd-none')
  })

  it('should add class submenu-scroll on scroll', () => {
    cy.viewport(480, 200)
    cy.get('.js-footer-content').scrollIntoView()

    cy.get('.js-submenu-section').should('have.class', 'submenu-scroll')
  })
})
