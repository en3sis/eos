const { strapiGraphql } = require('./strapi-graphql-query')

/* Initial middleware based on config file */
const strapiMiddleware = async (req, res, next) => {
  try {
    /* Define Querys for GraphQL */
    const strapiQuery = `applications { brand, showChangelog, enableFeedback, annoucements}, pages { title, description, path, enabled}`

    /* Get query result */
    const strapiQueryResponse = await strapiGraphql(strapiQuery)

    /* Remeber the path for future implementation */
    const requestRoute = req.originalUrl

    /* Reduce the route response to new object */
    const routes = await strapiQueryResponse.data.pages.reduce((acc, cur) => {
      /* acc = accumulator , cur = current item */
      acc[cur.path] = {
        title: cur.title,
        description: cur.description,
        enabled:
          process.env.NODE_ENV === undefined ||
          process.env.NODE_ENV === 'development'
            ? true
            : cur.enabled
      }

      return acc
    }, {})

    const docEnabled = await strapiQueryResponse.data.pages.reduce(
      (acc, cur) => {
        /* acc = accumulator , cur = current item */
        acc[cur.path] = {
          enabled:
            process.env.NODE_ENV === undefined ||
            process.env.NODE_ENV === 'development'
              ? true
              : cur.enabled
        }

        return acc
      },
      {}
    )

    /* Send the object with res.locals so we can use the props inside our views. */
    res.locals.config = strapiQueryResponse.data.applications[0]
    /* @TODO: Add resoueces via strapi, hardcoded it inhere because we removed the config.json  */
    res.locals.stylesheetURL =
      'https://gitlab.com/SUSE-UIUX/eos/blob/master/assets/stylesheets/base/variables/branding.scss'
    res.locals.docEnabled = docEnabled

    /* When the request is made, we check if the route corresponde with one of our defined routes, if they exist we create the locals for title and description. If we declare them outside this condition the API's would break */
    if (routes[requestRoute]) {
      const { title, description } = routes[requestRoute]
      res.locals.docTitle = title
      res.locals.docDescription = description
      res.locals.docPath = requestRoute
    } else {
      /* Set some default values for locals */
      res.locals.docTitle = 'EOS Design System'
      res.locals.docDescription = `{ SET DESCRIPTION }`
      res.locals.docPath = requestRoute
    }

    /* Handle next and redirect for route request, if the route is not defined we allow it */
    if (
      (routes[requestRoute] && routes[requestRoute].enabled) ||
      !routes[requestRoute]
    ) {
      next()
    } else {
      res.redirect('/')
    }
  } catch (error) {
    console.log('ERROR => projectConfig(): ', error.errors)
  }
}

module.exports = {
  strapiMiddleware
}
