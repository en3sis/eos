const express = require('express')
const router = express.Router()
const sass = require('node-sass')

/**
 * API Endpoint: /api/utils/scss
 * For the components to render, make sure the index.scss component files has an @import of the variables '../../eos-base/variables/index.scss';
 * If the @import is missing, the server will responde with an error.
 * For an example please check /assets/stylesheets/eos-elements/buttons/index.scss
 * @param components name of the componenent to be copiled
 * @example http://localhost:3000/api/utils/scss/?component=buttons
 */
router.get('/scss', async (req, res, next) => {
  const { component } = req.query

  sass.render(
    {
      file: `./assets/stylesheets/eos-elements/${component}/index.scss`,
      recursive: true,
      outputStyle: 'compressed'
    },
    function (err, result) {
      if (!err) {
        res.send(result.css.toString())
      } else {
        res.send(err)
      }
    }
  )
})

module.exports = router
