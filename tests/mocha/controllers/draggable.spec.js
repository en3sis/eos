describe('Draggable', () => {
  // create the demo element we need to test
  const draggableElement = $("<div class='js-draggable'></div")

  before((done) => {
    // we need to insert the button before the tests so we can test it
    $('body').append(draggableElement)
    done()
  })

  after((done) => {
    // this is only for the view of the tests, so it stays at top once it finished
    $(draggableElement).remove()
    done()
  })

  describe('translateElement', () => {
    it('Should move the element to a new position', () => {
      const target = $(draggableElement)[0]
      const x = 40
      const y = 20
      // call the method to change the original position of the element
      translateElement(target, x, y)
      expect($(target)).to.have.attr(
        'style',
        'transform: translate(' + x + 'px, ' + y + 'px);'
      )
      expect($(target)).to.have.data('x', x)
      expect($(target)).to.have.data('y', y)
    })
  })
})
